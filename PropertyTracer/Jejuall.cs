﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using PropertyTracer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

public class JejuallPropertyInfo
{
    public string 번호 { get; set; }
    public string 유형 { get; set; }
    public string 제목 { get; set; }
    public bool 서브프리미엄 { get; set; }
    public bool 중개업소프리미엄 { get; set; }
}
class Jejuall : DriverController
{
    protected AccInfo loginAcc;
    string loginUrl = "https://www.jejuall.com/CMember/login";
    public List<JejuallPropertyInfo> listPropertyInfo = new List<JejuallPropertyInfo>();

    public DataGridView listViewer;
    public Jejuall()
    {
        CreatDriver(TYPE_DRIVER.TYPE_Chrome , disableImg_: false);
#if !DEBUG
        driver.Manage().Window.Position = new System.Drawing.Point(-10000, -10000);
#endif
    }

    public bool GoLogin(AccInfo info_)
    {
        loginAcc = info_;
        driver.Navigate().GoToUrl(loginUrl);
        Thread.Sleep(3000);

        driver.FindElementByName("mem_id").SendKeys(info_.id);
        driver.FindElementByName("mem_pw").SendKeys(info_.pw + OpenQA.Selenium.Keys.Enter);
        Thread.Sleep(6000);

        return true;
    }


    public void CollectData(string _cate)
    {
        Global.uiController.PrintLog( $"{loginAcc.id} - {_cate} 데이터 수집");
        listPropertyInfo.Clear();

        //var url = $"https://www.jejuall.com/index.php/CMyProperty?page={i}";
        var url = $"https://www.jejuall.com/index.php/CMyProperty?category={Global.GetCateCode(_cate)}";
        driver.Navigate().GoToUrl(url);
        Thread.Sleep(1000);

        var list = driver.FindElements(By.XPath($"//*[contains(@id, 'property_loc_')]"));
        foreach(var item in list)
        {
            var id = item.GetAttribute("id");
            var cate = item.FindElement(By.ClassName("cate")).Text.Trim();
            var title = item.FindElements(By.ClassName("proname"))[1].Text.Trim();
            var item_cate = item.FindElement(By.ClassName("item_cate"));
            var subPrimeRoot = GetSubPrime(NextElem(item));
            //var tradePrimium = GetTradePrime(item_cate.FindElement(By.ClassName("premium")));
            var tradePrimium = false;

            //premium off

            listPropertyInfo.Add(new JejuallPropertyInfo
            { 
                번호 = id.Split('_')[2],
                유형 = cate,
                제목 = title,
                서브프리미엄 = subPrimeRoot,
                중개업소프리미엄 = tradePrimium,
            });
        }

        Global.uiController.PrintLog($"{loginAcc.id} : {Global.comboBox_Cate} / {listPropertyInfo.Count}");
        listViewer.Invoke(new MethodInvoker(delegate ()
        {
            listViewer.DataSource = null;
            listViewer.DataSource = listPropertyInfo;
            listViewer.Refresh();
        }));     
    }

    bool GetSubPrime(IWebElement _elem)
    {
        if (IsContainAsOutHtml(_elem, "premium off"))
        {
            return false;
        }
        else
        {
            if (IsContainAsOutHtml(_elem, "서브프리미엄"))
                return true;
            else
                return false;
        }            
    }
    bool GetTradePrime(IWebElement _elem)
    {
        if (IsContainAsOutHtml(_elem, "premium off"))
        {
            return false;
        }
        else
        {
            if (IsContainAsOutHtml(_elem, "중개업소프리미엄"))
                return true;
            else
                return false;
        }
    }
    void SetSubPrime(IWebElement _elem)
    {
        //*[@id="manager_t"]/tbody/tr[3]
        //*[@id="manager_t"]/tbody/tr[3]/td/div/a
        _elem.FindElement(By.XPath("./td/div/a")).Click();
        Thread.Sleep(1000);
    }
    void SetTradePrimium(IWebElement _elem)
    {
        //*[@id="manager_t"]/tbody/tr[3]
        //*[@id="manager_t"]/tbody/tr[3]/td/div/a
        _elem.FindElement(By.XPath("./td[3]/div/a")).Click();
        Thread.Sleep(1000);
        IsAlertPresent();
        Thread.Sleep(1000);
    }
    public void DoWorkProcess()
    {
        foreach(var cate in Global.listSeletecCate)
        {
            IList<string> windowHandles = new List<string>(driver.WindowHandles);
            driver.SwitchTo().Window(windowHandles[0]);

            CollectData(cate);
            try
            {
                driver.ExecuteScript("javascript:popupcookie('C');");
                Thread.Sleep(1000);
                Global.uiController.PrintLog("Close Popup");
            }
            catch
            {
                Global.uiController.PrintLog("Popup not exist");
            }
#if DEBUG
            //listPropertyInfo = listPropertyInfo.FindAll(x => x.서브프리미엄 == true);
#endif

            foreach (var item in listPropertyInfo)
            {
                if (Global.stop)
                    break;

                try
                {
                    windowHandles = new List<string>(driver.WindowHandles);
                    driver.SwitchTo().Window(windowHandles[0]);

                    var root = driver.FindElementById("manager_t");
                    var listTr = root.FindElements(By.XPath("./tbody/tr"));
                    var target = listTr.FirstOrDefault(x => x.GetAttribute("id").Contains(item.번호));
                    if (target != null)
                    {
                        var editBtn = target.FindElement(By.ClassName("btn_a"));
                        var newWin = new Actions(driver);
                        MoveScrollMiddle(editBtn);
                        newWin.KeyDown(OpenQA.Selenium.Keys.Shift).Click(editBtn).KeyUp(OpenQA.Selenium.Keys.Shift).Build().Perform();
                        Thread.Sleep(6000);

                        windowHandles = new List<string>(driver.WindowHandles);
                        driver.SwitchTo().Window(windowHandles[1]);

                        while (driver.PageSource.Contains("captcha-table"))
                        {
                            Global.uiController.PrintLog("capcha 감지");

                            Thread.Sleep(1000 * 50 * 30);

                            driver.Navigate().Refresh();
                            var msg = IsAlertPresent();
                            Global.uiController.PrintLog("msg");
                        }

                        var title = $"{item.번호}_{item.유형}";

                        var instance = new JejuallBase();
                        instance.dc = this;

                        instance.GetProcess(title);

                        driver.SwitchTo().Window(windowHandles[1]);
                        instance.SetProcess(title);

                        driver.SwitchTo().Window(windowHandles[0]);
                        var delBtn = target.FindElement(By.ClassName("btn_b"));
                        delBtn.Click();
                        Thread.Sleep(1000);

                        driver.FindElementById("alertify-ok").Click();
                        Thread.Sleep(3000);

                        driver.SwitchTo().Window(windowHandles[1]);
                        if (item.서브프리미엄)
                        {
                            Global.uiController.PrintLog("서브프리미엄 체크!");
                            SetSubPrime(driver.FindElementByXPath("//*[@id='manager_t']/tbody/tr[3]"));
                        }
                        if (item.중개업소프리미엄)
                        {
                            Global.uiController.PrintLog("중개업소프리미엄 체크!");
                            SetTradePrimium(driver.FindElementByXPath("//*[@id='manager_t']/tbody/tr[2]"));
                        }

                        driver.Close();

                        Random rand = new Random();
                        Thread.Sleep(1000 * (60 + rand.Next(60)));
                    }
                }
                catch
                {
                    windowHandles = new List<string>(driver.WindowHandles);
                    if (windowHandles.Count > 1)
                    {
                        driver.SwitchTo().Window(windowHandles[1]);
                        driver.Close();
                    }
                }              
            }

        }               
    }
}

