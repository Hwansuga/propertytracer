﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyTracer;
using System.Threading;

using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System.Text.RegularExpressions;

using System.Windows.Forms;
using OpenQA.Selenium.Support.UI;
using System.IO;
using System.Drawing;
using System.Net;
using PropertyTracer.Model;

public class PropertyInfo
{
    public string 제목 { get; set; }
    public string 번호 { get; set; }
    public string 유형 { get; set; }
    public string 위치 { get; set; }
    public string 이름 { get; set; }
    public string 가격 { get; set; }
    public string 메모 { get; set; }
}


class PropertyMgr : DriverController
{
    protected AccInfo loginAcc;

    protected string loginUrl = "https://ssl.jejukcr.com/membership/login?url=";

    string totalPageUrl = "http://land.jejukcr.com/my/offer?opt=all&state=0&page={0}";
    string greenPageUrl = "http://land.jejukcr.com/my/offer?opt=green&state=0&page={0}";

    protected string itemModifyUrl = "http://land.jejukcr.com/my/offer_post/{0}?state=0";

    public List<PropertyInfo> listPropertyInfo = new List<PropertyInfo>();

    public DataGridView listViewer = null;

    public PropertyMgr()
    {
        CreatDriver(TYPE_DRIVER.TYPE_Chrome, disableImg_: false);
#if !DEBUG
        driver.Manage().Window.Position = new System.Drawing.Point(-10000, -10000);
#endif
    }

    public virtual bool GoLogin(AccInfo info_)
    {
        loginAcc = info_;
        driver.Navigate().GoToUrl(loginUrl);
        Thread.Sleep(1000);

        driver.FindElementByName("id").SendKeys(info_.id);
        driver.FindElementByName("passwd").SendKeys(info_.pw + OpenQA.Selenium.Keys.Enter);
        Thread.Sleep(2000);

        if (driver.PageSource.Contains("등록되지 않은 아이디이거나"))
        {
            Global.uiController.PrintLog(info_.id + " 로그인 실패");
            return false;
        }
        else
        {
            Global.uiController.PrintLog(info_.id + " 로그인 성공");
            return true;
        }

        Thread.Sleep(Global.loginProcessSetting * 1000);      
    }

    bool CollectList()
    {
        IWebElement listRoot = driver.FindElementById("_offerManagement");
        IList<IWebElement> listItem = listRoot.FindElements(By.XPath("./table/tbody/tr"));
        foreach (var item in listItem)
        {
            string html = item.GetAttribute("innerHTML");
            if (html.Contains("매물이 없습니다"))
                return false;

            if (html.Contains("유료광고"))
                continue;

            if (html.Contains("M프리미엄"))
                continue;

            //*[@id="_offerManagement"]/table/tbody/tr[1]/td[4]
            //*[@id="_offerManagement"]/table/tbody/tr[1]/td[4]/div/a/span[1]
            string title = item.FindElement(By.ClassName("title")).FindElement(By.XPath("./div/a/span[1]")).Text;
            string num = item.GetAttribute("data-idx");
            string localtion = item.FindElement(By.ClassName("address")).Text.Replace("\r\n", " ");
            string type = item.FindElement(By.ClassName("cate")).Text.Replace(" ", "");
            string name = item.FindElement(By.ClassName("detail_info")).Text;
            string price = item.FindElement(By.ClassName("priceview")).Text.Replace(",","");
            List<string> listNum = Regex.Split(price, @"\D+").ToList().FindAll(x=> string.IsNullOrEmpty(x) == false);
            string newPrice = string.Join("/", listNum.ToArray());

            listPropertyInfo.Add(new PropertyInfo() {제목 = title,  번호 = num, 위치 = localtion.Trim(), 유형 = type.Trim(), 가격 = newPrice, 이름 = name.Trim() });
        }

        //testcode
        //listPropertyInfo = listPropertyInfo.GetRange(0, 1);

        return true;
    }
    public void SaveCollectDataTotxt()
    {
        List<string> listData = new List<string>();
        listData = listPropertyInfo.ConvertAll<string>(x => x.번호 + "/" + x.유형 + "/" + x.위치 + "/" + x.이름 + "/" + x.가격);

        string saveName = loginAcc.id + "_" + DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss") + "_CollectList";

        Util.SaveTxtFile(saveName + ".txt" , listData);
    }

    public void CollectTotalData()
    {
        Global.uiController.PrintLog(loginAcc.id + " : 데이터 수집");

        string pageUrl = string.Format(totalPageUrl, 1);
        driver.Navigate().GoToUrl(pageUrl);
        Thread.Sleep(1000);


        //*[@id="_offerManagement"]/div
        IWebElement page_navi = driver.FindElementByXPath("//*[@id='_offerManagement']/div");
        IList<IWebElement> navis = page_navi.FindElements(By.XPath(".//*"));

        listPropertyInfo.Clear();
        CollectList();
        if (navis.Count >= 2)
        {
            for (int i = 2; i <= navis.Count; ++i)
            {
                Thread.Sleep(Global.pageSearchSetting * 1000);

                pageUrl = string.Format(totalPageUrl, i);
                driver.Navigate().GoToUrl(pageUrl);
                Thread.Sleep(1000);

                if (CollectList() == false)
                    break;                
            }
        }

        Global.uiController.PrintLog(loginAcc.id + " : " + listPropertyInfo.Count);

        listViewer?.Invoke(new MethodInvoker(delegate () {
            listViewer.DataSource = null;
            listViewer.DataSource = listPropertyInfo;
            listViewer.Refresh();
        }));

        SaveCollectDataTotxt();
    }

    public virtual void CollectGreenData()
    {
        Global.uiController.PrintLog(loginAcc.id + " : 데이터 수집");

        listPropertyInfo.Clear();
        for (int i=1; i<=2; ++i)
        {
            var url = string.Format(greenPageUrl, i);
            driver.Navigate().GoToUrl(url);
            Thread.Sleep(1000);
            CollectList();
        }
        

        Global.uiController.PrintLog(loginAcc.id + " : " + listPropertyInfo.Count);

        listViewer?.Invoke(new MethodInvoker(delegate ()
        {
            listViewer.DataSource = null;
            listViewer.DataSource = listPropertyInfo;
            listViewer.Refresh();
        }));
       
    }

    protected virtual bool CheckLoginSession()
    {
        Global.uiController.PrintLog(loginAcc.id + " 로그인 세션 체크");
        string pageUrl = string.Format(totalPageUrl, 1);
        driver.Navigate().GoToUrl(pageUrl);
        Thread.Sleep(1000);

        if (driver.PageSource.Contains("회원가입"))
        {
            Global.uiController.PrintLog(loginAcc.id + " 로그인 세션이 끊겨있음 재 로그인 시도");
            return GoLogin(loginAcc);
        }

        return true;
    }

    public virtual void RegistNewItem(PropertyInfo itemInfo_)
    {
        //
        var modify = string.Format(itemModifyUrl, itemInfo_.번호);
        driver.Navigate().GoToUrl(modify);
        Thread.Sleep(1000);

        var className = GetSelectedValueFromDropdown(By.Name("cateid")).Trim().Replace("/","_");
        var instance = Activator.CreateInstance(Type.GetType(className));
        if (instance == null)
        {
            Global.uiController.PrintLog($"Does not exist class : {className}");
        }
        else
        {
            var item = instance as PropertyBase;
            item.dc = this;
            item.Process(itemInfo_);
        }     
    }

    public virtual void DoWorkProcess()
    {
        if (CheckLoginSession() == false)
        {
            Global.uiController.PrintLog("로그인 세션 유지 실패");
            return;
        }

        //CollectTotalData();
        CollectGreenData();

#if DEBUG
        //listPropertyInfo = listPropertyInfo.FindAll(x=>x.유형.Contains("상가건물"));
        //listPropertyInfo = listPropertyInfo.Skip(30).ToList();
#endif

        //listPropertyInfo.Reverse();


        foreach (var item in listPropertyInfo)
        {
            if (Global.stop)
                break;

#if DEBUG
            this.RegistNewItem(item);
#else
            try
            {
                this.RegistNewItem(item);
            }
            catch
            {
                Global.uiController.PrintLog($"{item.번호} {item.제목} 작업 실패.. 입력 포멧에 대한 확인 필요");
            }
#endif
        }
    }
}

