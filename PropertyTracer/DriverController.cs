﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;


public enum TYPE_DRIVER
{
    TYPE_Chrome = 0,
    TYPE_IE = 1,
}

class DriverController
{
    public ChromeDriver driver;

    public string IsAlertPresent(bool accept_ = true)
    {
        try
        {
            string msg = driver.SwitchTo().Alert().Text;
            if (accept_)
                driver.SwitchTo().Alert().Accept();
            else
                driver.SwitchTo().Alert().Dismiss();
            return msg;
        }
        catch
        {
            return "";
        }

        Thread.Sleep(1000);
    }  
    
    public void GetCookieFromDriver(DriverController otherDrvier_)
    {
        if (driver == null)
            return;

        foreach (var item in otherDrvier_.driver.Manage().Cookies.AllCookies)
            driver.Manage().Cookies.AddCookie(item);
    } 

    public void MoveScroll(IWebElement element_)
    {
        string strPosOrder = string.Format("window.scrollTo({0},{1})", element_.Location.X, element_.Location.Y - 100);
        driver.ExecuteScript(strPosOrder, "");

        //System.Threading.Thread.Sleep(100);
    }

    public void MoveScrollMiddle(IWebElement element_)
    {
        // Get the element's X and Y coordinates
        int elementX = element_.Location.X;
        int elementY = element_.Location.Y;

        // Adjust the Y coordinate to bring the element closer to the middle of the screen
        string script = @"
        let elementY = arguments[0];
        let windowHeight = window.innerHeight;
        window.scrollTo({left: 0, top: elementY - (windowHeight / 2)});
    ";
        driver.ExecuteScript(script, elementY);
    }

    public IList<IWebElement> FindMyElements(By by)
    {
        WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
        wait.Until(ExpectedConditions.ElementExists(by));
        return driver.FindElements(by);
    }

    public IWebElement FindMyElement(By by)
    {
        WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
        wait.Until(ExpectedConditions.ElementExists(by));
        IWebElement element = driver.FindElement(by);
        MoveScroll(element);
        return element;
    }
    public IWebElement ClickElement(By by, bool moveScrol_ = true)
    {
        IWebElement element = driver.FindElement(by);
        if (moveScrol_)
            MoveScroll(element);
        WaitUntilExist(by);
        element.Click();
        System.Threading.Thread.Sleep(100);
        return element;
    }

    public void ClickElement(IWebElement element_, bool moveScrol_ = true , int sleepTime_ = 500)
    {
        if(moveScrol_)
        {
            MoveScroll(element_);
            Thread.Sleep(sleepTime_);
        }           
        element_.Click();
        System.Threading.Thread.Sleep(sleepTime_);
    }

    public void QuiteDriver()
    {
        if (driver == null)
            return;

        driver.Quit();
        driver = null;
    }

    public void LoadPageComplete()
    {
        new WebDriverWait(driver, TimeSpan.FromSeconds(5)).Until(d => ((IJavaScriptExecutor)d).ExecuteScript("return document.readyState").Equals("complete"));
    }

    public void WaitUntilExist(By by, int sleepTime = 1000)
    {
        try
        {           
            Thread.Sleep(sleepTime);
            LoadPageComplete();
            new WebDriverWait(driver, TimeSpan.FromSeconds(5)).Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(by));
            Thread.Sleep(500);
        }
        catch
        {
            return;
        }
    }

    public void CreatDriver(TYPE_DRIVER type_ = TYPE_DRIVER.TYPE_Chrome, bool showBrowser_ = true, bool disableImg_ = true, bool _typeDebug = false)
    {
        QuiteDriver();

        switch (type_)
        {
            case TYPE_DRIVER.TYPE_Chrome:
                Create_Chrome(showBrowser_, disableImg_, _typeDebug);
                break;
        }
    }

    public bool IsInPage(string msg_)
    {
        if (driver.PageSource.Contains(msg_))
            return true;
        return false;
    }

    public void Create_Chrome(bool showBrowser_, bool disableImg_ = true, bool _typeDebug = false)
    {
        var driverService = ChromeDriverService.CreateDefaultService();
        driverService.HideCommandPromptWindow = true;

        var options = new ChromeOptions();

        if (showBrowser_ == false)
        {
            options.AddArgument("--headless");
            options.AddUserProfilePreference("profile.default_content_setting_values.images", 2);
        }
        options.AddArgument("--disable-blink-features=AutomationControlled");
        options.AddArgument("user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.88 Safari/537.36");


        if (_typeDebug)
        {
            options.AddArgument("--ignore-ssl-errors=yes");
            options.AddArgument("--ignore-certificate-errors");

            options.DebuggerAddress = "127.0.0.1:9222";

            if (Directory.Exists(@"C:\chrometemp") == false)
                Directory.CreateDirectory(@"C:\chrometemp");

            var chromProcess = new Process
            {
                StartInfo =
        {
            //FileName = @"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe",
            FileName = Util.GetChomePath(),
            Arguments = $"--remote-debugging-port=9222 --user-data-dir=C:\\chrometemp\\"
        }
            };
            chromProcess.Start();
            Thread.Sleep(3000);

        }
        else
        {
            //         ChromePerformanceLoggingPreferences logPrefs = new ChromePerformanceLoggingPreferences();
            //         options.PerformanceLoggingPreferences = logPrefs;
            options.SetLoggingPreference("performance", LogLevel.All);

            options.AddArgument("no-sandbox");
            options.AddArgument("--disable-popup-blocking");
            options.AddArgument("--disable-infobars");
            //options.AddArgument("--incognito");
            options.AddArgument("--disable-extensions");
            options.AddArgument("--disable-notifications");
            options.AddArgument("--silent");
            options.AddArgument("-js-flags=--expose-gc");
            options.AddArgument("--ignore-ssl-errors=yes");
            options.AddArgument("--ignore-certificate-errors");
            //options.AddArgument("user-data-dir=C:\\selenium");
            if (disableImg_)
            {
                options.AddArgument("--blink-settings=imagesEnabled=false");
                options.AddArgument("--disable-images");
            }



            //options.AddArgument("--disable-gpu");
            options.AddArgument("--window-size=1800,1080");
            //options.AddArgument("--lang=ko_KR");
            //options.AddExtension("Block-image_v1.0.crx");
            //options.AddArgument("--blink-settings=imagesEnabled=false");
            //options.AddArgument("--disable-images");

            //string CacheDir = Application.StartupPath + @"\PCUserData\";
            //options.AddArgument(string.Concat("--disk-cache-dir=" + CacheDir + "cache"));
        }

        driver = new ChromeDriver(driverService, options);
        driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(90);
        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

    }

    public void MouseOverAction(By path_)
    {
        IWebElement menu = driver.FindElement(path_);
        Actions builder = new Actions(driver);
        builder.MoveToElement(menu).Build().Perform();
    }

    public void MouseOverAction(IWebElement webElem_)
    {
        Actions builder = new Actions(driver);
        builder.MoveToElement(webElem_).Build().Perform();
    }

    public bool FocusWindow(string keyWord_)
    {
        ReadOnlyCollection<string> tabWindows = driver.WindowHandles;
        foreach (string tabItem in tabWindows)
        {
            if (driver.SwitchTo().Window(tabItem).Title == keyWord_)
                return true;
        }

        return false;
    }

    public bool IsContainAsHtml(IWebElement webElem_,string keyWord_)
    {
        string innerHTML = webElem_.GetAttribute("innerHTML");
        if (innerHTML.Contains(keyWord_))
            return true;
        return false;
    }

    public bool IsContainAsOutHtml(IWebElement webElem_, string keyWord_)
    {
        string innerHTML = webElem_.GetAttribute("outerHTML");
        if (innerHTML.Contains(keyWord_))
            return true;
        return false;
    }

    public IWebElement GetParent(IWebElement e)
    {
        return e.FindElement(By.XPath(".."));
    }

    public void SelectComboBox(IWebElement elem_ , string item_)
    {
        var selectElement = new SelectElement(elem_);
        selectElement.SelectByText(item_);
        Thread.Sleep(1000);
    }

    public void SelectComboBox(By _by, string _item)
    {
        if (string.IsNullOrEmpty(_item))
            return;

        SelectComboBox(driver.FindElement(_by),_item);
    }
    public void SelectComboBoxByValue(By _by, string _item)
    {
        if (string.IsNullOrEmpty(_item))
            return;

        var selectElement = new SelectElement(driver.FindElement(_by));
        selectElement.SelectByValue(_item);
        Thread.Sleep(2000);
    }

    public void SelectComboBoxByText(By _by, string _item)
    {
        var selectElement = new SelectElement(driver.FindElement(_by));
        try
        {
            selectElement.SelectByText(_item);
        }
        catch
        {
            selectElement.Options.Last().Click();
        }
        
        Thread.Sleep(2000);
    }

    public void SetValue(By _by, string _value, bool _clear = false)
    {
        if (string.IsNullOrEmpty(_value))
            return;

        var elem = driver.FindElement(_by);
        if (elem.Displayed)
        {
            if (_clear)
            {
                driver.FindElement(_by).Clear();
                Thread.Sleep(1000);
            }           
            
            driver.FindElement(_by).SendKeys(_value);
            Thread.Sleep(2000);              
        }
    }

    public void SetValueByJS(By _by, string _value)
    {
        string script = $"arguments[0].value = '{_value}';";

        driver.ExecuteScript(script , driver.FindElement(_by));
        Thread.Sleep(1000);
    }

    public string GetSelectedValueFromDropdown(By _by)
    {
        var select = new SelectElement(driver.FindElement(_by));
        var option = select.SelectedOption;
        return option.Text;
    }

    public void RemoveDisplayNoneElem()
    {
        string removeElementsScript = @"
                var elements = document.querySelectorAll('*');
                for (var i = elements.length - 1; i >= 0; i--) {
                    var element = elements[i];
                    var style = window.getComputedStyle(element);
                    if (style.display === 'none') {
                        element.parentNode.removeChild(element);
                    }
                }
            ";
        driver.ExecuteScript(removeElementsScript);
    }

    public bool GetCheckBox(By _by)
    {
        return driver.FindElement(_by).Selected;
    }
    public void SetCheckBox(By _by , bool _value)
    {
        var elem = driver.FindElement(_by);
        if (elem.Displayed == false)
            return;

        if (elem.Selected == _value)
            return;

        elem.Click();
        Thread.Sleep(1000);
    }

    public IWebElement NextElem(IWebElement _elem)
    {
        return _elem.FindElement(By.XPath("following-sibling::*[1]"));
    }

    public string GetValueByName(string _name)
    {
        if (driver.PageSource.Contains($"name=\"{_name}\""))
            return driver.FindElementByName(_name).GetAttribute("value");
        else
            return string.Empty;
    }

}

