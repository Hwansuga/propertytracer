﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PropertyTracer
{
    public partial class Form1 : Form
    {
        List<Thread> listWorkThread = new List<Thread>();
        public Form1()
        {
            InitializeComponent();

            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls13;
        }

        public void PrintLog(string msg_)
        {
            listBox_Log.Invoke(new MethodInvoker(delegate ()
            {
                Util.AutoLineStringAdd(listBox_Log, msg_);
            }));
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //Util.KillProcess("chrome");

            Text = "부동산교차로-Ver1.0";
#if JEJUALL
            Text = "오일장-Ver1.0";
            checkedListBox_Cate.Visible = true;
            foreach (var item in Global.dicCate)
                checkedListBox_Cate.Items.Add(item.Key);
#if DEBUG
            //comboBox_Cate.SelectedItem = Global.comboBox_Cate = "기타";
#endif
#else
            checkedListBox_Cate.Visible = false;
#endif

#if DEBUG
            Util.KillProcess("chromedriver");
#else
            Util.KillProcess("chrome");
#endif
//             var data = File.ReadAllText("빌라연립다세대.json");
//             Global.list빌라연립다세대 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<빌라연립다세대>>(data);


            EventUtil.ConnectCustomEvent(this);
            Global.uiController = this;

            Global.LoadAccInfo();
            Global.LoadSettingFile();

            button_Start.Enabled = true;

            button_Stop.Enabled = false;
        }

        void DoneToStop()
        {
            this.Invoke(new MethodInvoker(delegate () {
                button_Stop.Enabled = false;
                button_Start.Enabled = true;
                numericUpDown_DelayTime.Enabled = true;
            }));

            listWorkThread.Clear();
            
            PrintLog("완료");
        }

        void StepWorkThread()
        {
            for (int k = 0; k < Global.numericUpDown_PlayTimes; ++k)
            {
                
                if (Global.stop)
                    break;

                PrintLog($"{k + 1}번째 시작");

                List<DataGridView> listView = Util.GetFieldListByName<DataGridView>(this, "dataGridView_ListProperty");
                foreach (var item in Global.listAccInfo)
                {
                    if (Global.stop)
                        break;

                    PrintLog(item.id + " 로그인 시도");
                    //var className = (item.cate == "jejukcr") ? "PropertyMgr" : item.cate;
                    //var mgr = Activator.CreateInstance(Type.GetType(className)) as PropertyMgr;
#if JEJUALL
                    var mgr = new Jejuall();
#else
                    var mgr = new PropertyMgr();
#endif
                    if (mgr.GoLogin(item) == false)
                    {
                        PrintLog(item.id + " 로그인 실패");
                        continue;
                    }

                    mgr.listViewer = listView[0];

                    PrintLog(item.id + " 작업시작");
                    
                    mgr.DoWorkProcess();
                    Thread.Sleep(1000);                   

                    mgr.QuiteDriver();
                }

                PrintLog(" 대기시간 : " + Global.numericUpDown_DelayTime);
                Thread.Sleep(Global.numericUpDown_DelayTime * 1000);

            }

            DoneToStop();
        }


        private void button_Start_Click(object sender, EventArgs e)
        {
#if JEJUALL
            Global.listSeletecCate.Clear();
            foreach (var selected in checkedListBox_Cate.CheckedItems)
                Global.listSeletecCate.Add(selected.ToString());
            if (Global.listSeletecCate.Count <= 0)
            {
                PrintLog($"카테고리를 설정해 주세요");
                return;
            }
                
#endif

            button_Start.Enabled = false;
            numericUpDown_DelayTime.Enabled = false;

            button_Stop.Enabled = true;
            Global.stop = false;

#if JEJUALL
            if (listWorkThread.Count > 0)
            {
                PrintLog("재 시작");
                foreach (var item in listWorkThread)
                    item.Resume();

                button_Stop.Enabled = true;
            }
            else
            {

                PrintLog("시작");
                //MakeWorker();

                button_Stop.Enabled = true;
                Thread worker = new Thread(StepWorkThread);
                worker.Start();
            }   
#else

            var tWorker = Task.Run(async () => {
                var tasks = new List<Task>();
                foreach (var item in Global.listAccInfo)
                {
                    var acc = item; // 클로저 캡처 문제 방지
                    var t = Task.Run(() => {
                        for (int k = 0; k < Global.numericUpDown_PlayTimes; ++k)
                        {
                            if (Global.stop)
                                break;
                            var mgr = new PropertyMgr();

                            if (mgr.GoLogin(item) == false)
                            {
                                PrintLog(item.id + " 로그인 실패");
                                continue;
                            }

                            //mgr.listViewer = listView[0];

                            PrintLog(item.id + " 작업시작");

                            mgr.DoWorkProcess();
                            Thread.Sleep(1000);

                            mgr.QuiteDriver();
                        }
                    });

                    tasks.Add(t);
                }

                await Task.WhenAll(tasks);
                DoneToStop();
            });

            
#endif
        }

        private void button_Stop_Click(object sender, EventArgs e)
        {
            PrintLog("정지중...");
            button_Stop.Enabled = false;
            Global.stop = true;
        }


        private void button_SaveLog_Click(object sender, EventArgs e)
        {
            this.Invoke(new MethodInvoker(delegate() {
                List<string> logData = this.listBox_Log.Items.OfType<string>().ToList();
                Util.SaveTxtFile($"{DateTime.Now.ToLongDateString()}_LogData.txt", logData);

                PrintLog("저장 완료");
            }));
        }
    }
}
