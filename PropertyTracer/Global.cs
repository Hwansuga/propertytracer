﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyTracer;
using System.Windows.Forms;
using System.IO;

class AccInfo
{
    public string cate { get; set; }
    public string id { get; set; }
    public string pw { get; set; }
}

class Global : Singleton<Global>
{
    public static bool stop = false;

    public static int numericUpDown_DelayTime = 1;
    public static string comboBox_Cate = string.Empty;

    public static Form1 uiController;

    public static List<AccInfo> listAccInfo = new List<AccInfo>();
    
    public static readonly int limitAccCnt = 100;

    public static int loginProcessSetting = 1;
    public static int pageSearchSetting = 3;
    public static int itemProcessSetting = 4;

    public static int clickKeepBtnSetting = 5;
    public static int clickSimilarBtnSetting = 1;
    public static int clickSumitBntSetting = 1;
    public static int numericUpDown_PlayTimes = 2;

    public static Dictionary<string, string> dicCate = new Dictionary<string, string>{
        {"아파트" , "PR01" },
        {"단독다가구" , "PR02" },
        {"빌라연립다세대" , "PR03" },
        {"원룸투룸" , "PR04_1" },
        {"오피스텔/도시형" , "PR05" },
        {"상가점포" , "PR06" },
        {"상가주택" , "PR07" },
        {"상가건물" , "PR08" },
        {"숙박콘도펜션" , "PR09" },
        {"토지임야" , "PR11" },
        {"기타" , "PR16" },
    };

    public static List<string> listSeletecCate = new List<string>();

    public static string GetSelectedCateCode()
    {
        return dicCate[comboBox_Cate];
    }

    public static string GetCateCode(string _key)
    {
        return dicCate[_key];
    }

    public static void LoadAccInfo()
    {

        /*
        var fileName = "AccInfo.json";
        
        listAccInfo.Add(new AccInfo
        {
            cate = "jejukcr",
            id = "sunsun3832",
            pw = "@boosi0887"
        });
        listAccInfo.Add(new AccInfo
        {
            cate = "Jejuall",
            id = "sunsun3832",
            pw = "boosi0887"
        });
        File.WriteAllText(fileName, Newtonsoft.Json.JsonConvert.SerializeObject(listAccInfo , Newtonsoft.Json.Formatting.Indented));
        

        if (File.Exists(fileName) == false)
        {
            MessageBox.Show($"{fileName} 안에 계정 정보를 기입해 주세요");
            return;
        }
        listAccInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AccInfo>>(File.ReadAllText(fileName));
        */

        var fileName = "AccInfo.txt";
#if DEBUG && JEJUALL
        fileName = "AccInfo_JejuAll.txt";
#endif

        List<string> listData = new List<string>();
        Util.LoadTxtFile(fileName, ref listData);
        listAccInfo.Clear();
        foreach (var item in listData)
        {
            string[] _data = item.Split('/');
            listAccInfo.Add(new AccInfo() { id = _data[0], pw = _data[1] });
        }
        if (limitAccCnt < listAccInfo.Count)
            listAccInfo = listAccInfo.GetRange(0, limitAccCnt);
        if (listAccInfo.Count <= 0)
            MessageBox.Show("AccInfo.txt 안에 계정 정보를 기입해 주세요");
    }

    public static void LoadSettingFile()
    {
        Util.LoadValueInIni(Global.Instance, "Setting.ini", "", "Setting");
    }
}

