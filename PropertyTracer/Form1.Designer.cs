﻿namespace PropertyTracer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button_Start = new System.Windows.Forms.Button();
            this.button_Stop = new System.Windows.Forms.Button();
            this.dataGridView_ListProperty1 = new System.Windows.Forms.DataGridView();
            this.listBox_Log = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDown_DelayTime = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.button_SaveLog = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.numericUpDown_PlayTimes = new System.Windows.Forms.NumericUpDown();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.checkedListBox_Cate = new System.Windows.Forms.CheckedListBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_ListProperty1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_DelayTime)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_PlayTimes)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // button_Start
            // 
            this.button_Start.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_Start.Location = new System.Drawing.Point(0, 0);
            this.button_Start.Name = "button_Start";
            this.button_Start.Size = new System.Drawing.Size(134, 21);
            this.button_Start.TabIndex = 2;
            this.button_Start.Text = "작업 시작";
            this.button_Start.UseVisualStyleBackColor = true;
            this.button_Start.Click += new System.EventHandler(this.button_Start_Click);
            // 
            // button_Stop
            // 
            this.button_Stop.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_Stop.Location = new System.Drawing.Point(0, 21);
            this.button_Stop.Name = "button_Stop";
            this.button_Stop.Size = new System.Drawing.Size(134, 21);
            this.button_Stop.TabIndex = 3;
            this.button_Stop.Text = "작업 정지";
            this.button_Stop.UseVisualStyleBackColor = true;
            this.button_Stop.Click += new System.EventHandler(this.button_Stop_Click);
            // 
            // dataGridView_ListProperty1
            // 
            this.dataGridView_ListProperty1.AllowUserToAddRows = false;
            this.dataGridView_ListProperty1.AllowUserToDeleteRows = false;
            this.dataGridView_ListProperty1.AllowUserToResizeColumns = false;
            this.dataGridView_ListProperty1.AllowUserToResizeRows = false;
            this.dataGridView_ListProperty1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView_ListProperty1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView_ListProperty1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_ListProperty1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dataGridView_ListProperty1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView_ListProperty1.Location = new System.Drawing.Point(134, 27);
            this.dataGridView_ListProperty1.Name = "dataGridView_ListProperty1";
            this.dataGridView_ListProperty1.RowHeadersVisible = false;
            this.dataGridView_ListProperty1.RowTemplate.Height = 23;
            this.dataGridView_ListProperty1.Size = new System.Drawing.Size(460, 233);
            this.dataGridView_ListProperty1.TabIndex = 4;
            // 
            // listBox_Log
            // 
            this.listBox_Log.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.listBox_Log.FormattingEnabled = true;
            this.listBox_Log.ItemHeight = 12;
            this.listBox_Log.Location = new System.Drawing.Point(134, 260);
            this.listBox_Log.Name = "listBox_Log";
            this.listBox_Log.Size = new System.Drawing.Size(460, 160);
            this.listBox_Log.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(140, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 12);
            this.label1.TabIndex = 6;
            this.label1.Text = "::마이 부동산::";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 357);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 10;
            this.label2.Text = "대기시간";
            // 
            // numericUpDown_DelayTime
            // 
            this.numericUpDown_DelayTime.Location = new System.Drawing.Point(0, 372);
            this.numericUpDown_DelayTime.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_DelayTime.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_DelayTime.Name = "numericUpDown_DelayTime";
            this.numericUpDown_DelayTime.Size = new System.Drawing.Size(77, 21);
            this.numericUpDown_DelayTime.TabIndex = 11;
            this.numericUpDown_DelayTime.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(79, 374);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 12);
            this.label3.TabIndex = 12;
            this.label3.Text = "분";
            // 
            // button_SaveLog
            // 
            this.button_SaveLog.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.button_SaveLog.Location = new System.Drawing.Point(0, 399);
            this.button_SaveLog.Name = "button_SaveLog";
            this.button_SaveLog.Size = new System.Drawing.Size(134, 21);
            this.button_SaveLog.TabIndex = 13;
            this.button_SaveLog.Text = "로그 저장";
            this.button_SaveLog.UseVisualStyleBackColor = true;
            this.button_SaveLog.Click += new System.EventHandler(this.button_SaveLog_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.checkedListBox_Cate);
            this.panel1.Controls.Add(this.numericUpDown_PlayTimes);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.button_SaveLog);
            this.panel1.Controls.Add(this.button_Stop);
            this.panel1.Controls.Add(this.button_Start);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.numericUpDown_DelayTime);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(134, 420);
            this.panel1.TabIndex = 14;
            // 
            // numericUpDown_PlayTimes
            // 
            this.numericUpDown_PlayTimes.Dock = System.Windows.Forms.DockStyle.Top;
            this.numericUpDown_PlayTimes.Location = new System.Drawing.Point(0, 64);
            this.numericUpDown_PlayTimes.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_PlayTimes.Name = "numericUpDown_PlayTimes";
            this.numericUpDown_PlayTimes.Size = new System.Drawing.Size(134, 21);
            this.numericUpDown_PlayTimes.TabIndex = 16;
            this.numericUpDown_PlayTimes.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label4);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 42);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(134, 22);
            this.panel2.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(35, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 14;
            this.label4.Text = "횟수";
            // 
            // checkedListBox_Cate
            // 
            this.checkedListBox_Cate.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkedListBox_Cate.FormattingEnabled = true;
            this.checkedListBox_Cate.Location = new System.Drawing.Point(0, 85);
            this.checkedListBox_Cate.Name = "checkedListBox_Cate";
            this.checkedListBox_Cate.Size = new System.Drawing.Size(134, 180);
            this.checkedListBox_Cate.TabIndex = 18;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(594, 420);
            this.Controls.Add(this.dataGridView_ListProperty1);
            this.Controls.Add(this.listBox_Log);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "부동산교차로-Ver1.0";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_ListProperty1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_DelayTime)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_PlayTimes)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button_Start;
        private System.Windows.Forms.Button button_Stop;
        private System.Windows.Forms.DataGridView dataGridView_ListProperty1;
        private System.Windows.Forms.ListBox listBox_Log;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericUpDown_DelayTime;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button_SaveLog;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.NumericUpDown numericUpDown_PlayTimes;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckedListBox checkedListBox_Cate;
    }
}

