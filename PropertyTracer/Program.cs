﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PropertyTracer
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            string mutexKey = "{F45A0687-12F8-4459-83B7-2C30B697C8FA}";

            //check duplication
            if (Util.CheckDuplication(mutexKey))
                return;

//             if (Util.CheckExpireDate(new DateTime(2019, 5, 31)))
//             {
//                 MessageBox.Show("기간이 만료되었습니다.");
//                 return;
//             }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
