﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PropertyTracer.Model
{
    class PropertyBase
    {
        public DriverController dc = null;
        string itemModifyUrl = "http://land.jejukcr.com/my/offer_post/{0}?state=0";
        string itemUrl = "http://land.jejukcr.com/offer/{0}?state=0";
        string regUrl = "http://land.jejukcr.com/my/offer_reg";

        public string 매물종류 { get; set; }
        public string 거래유형 { get; set; }
        public string 매물소재지 { get; set; }
        public string 면적 { get; set; }
        public string 동 { get; set; }
        public string 호 { get; set; }
        public string 지도표시 { get; set; } = string.Empty;
        public string 해당층 { get; set; }
        public string 총층 { get; set; }
        public string 지상층 { get; set; }
        public string 지하층 { get; set; }

        public string 매매가 { get; set; }
        public string 전세금 { get; set; }
        public string 보증금 { get; set; }
        public string 년세금 { get; set; }
        public string 월세금 { get; set; }
        public string 월관리비 { get; set; }
        public string 관리비포함내역 { get; set; } = string.Empty;

        //매물정보
        public string 지목 { get; set; }
        public string 연면적 { get; set; }
        public string 현업종 { get; set; }
        public string 대지면적 { get; set; }
        public string 건물종류 { get; set; }
        public string 공급면적 { get; set; }
        public string 계약면적 { get; set; }
        public string 전용면적 { get; set; }
        public string 건물명 { get; set; }
        public string 용도 { get; set; }
        public string 건축물용도 { get; set; }
        public string 방향 { get; set; }
        public string 입주가능일 { get; set; }
        public string 방 { get; set; }
        public string 욕실 { get; set; }
        public string 총세대수 { get; set; }
        public string 건축물일자 { get; set; }
        public string 총주차대수 { get; set; }


        public string 간략소개 { get; set; }
        public string 상세설명 { get; set; }
        public string 메모 { get; set; }


        public virtual void GetBasicInfo()
        {         
            매물종류 = dc.GetSelectedValueFromDropdown(By.Name("cateid")).Trim();

            //var tradeValue = dc.driver.FindElementByName("trade").GetAttribute("value");
            var listTradBtn = dc.driver.FindElementsByClassName("trade_btn");
            var targetBtn = listTradBtn.FirstOrDefault(x => x.GetAttribute("outerHTML").Contains(" on"));
            거래유형 = targetBtn.Text.Trim();

            var map_state_option = dc.driver.FindElementByClassName("map_state_option");
            if(map_state_option.GetAttribute("data-checked-value") == "y")
            {
                var listOption = map_state_option.FindElements(By.ClassName("cof-radio-item"));
                var optionOn = listOption.FirstOrDefault(x => x.GetAttribute("outerHTML").Contains(" on"));
                지도표시 = optionOn.Text.Trim();
            }


            GetAddress();



            if (dc.driver.PageSource.Contains("name=\"dong\""))
                동 = dc.driver.FindElementByName("dong").GetAttribute("value");
            if (dc.driver.PageSource.Contains("name=\"ho\""))
                호 = dc.driver.FindElementByName("ho").GetAttribute("value");
            if (dc.driver.PageSource.Contains("name=\"t_current_floor\""))
                해당층 = dc.driver.FindElementByName("t_current_floor").GetAttribute("value");
            if (dc.driver.PageSource.Contains("name=\"t_total_floor\""))
                총층 = dc.driver.FindElementByName("t_total_floor").GetAttribute("value");
            if (dc.driver.PageSource.Contains("name=\"total_floor\""))
                지상층 = dc.driver.FindElementByName("total_floor").GetAttribute("value");
            if (dc.driver.PageSource.Contains("name=\"current_floor\""))
                지하층 = dc.driver.FindElementByName("current_floor").GetAttribute("value");

            if (dc.driver.PageSource.Contains("name=\"sale_price\""))
            {
                매매가 = dc.driver.FindElementByName("sale_price").GetAttribute("value");
            }

            if (dc.driver.PageSource.Contains("name=\"deposit\""))
            {
                var elem = dc.driver.FindElementByName("deposit");
                if (dc.IsContainAsOutHtml(elem,"전세금"))
                    전세금 = elem.GetAttribute("value");
                else if (dc.IsContainAsOutHtml(elem, "보증금"))
                    보증금 = elem.GetAttribute("value");
            }

            if (dc.driver.PageSource.Contains("name=\"monthly_rent\""))
            {
                var elem = dc.driver.FindElementByName("monthly_rent");
                if (dc.IsContainAsOutHtml(elem, "년세금"))
                    년세금 = elem.GetAttribute("value");
                else if (dc.IsContainAsOutHtml(elem, "월세금"))
                    월세금 = elem.GetAttribute("value");
            }

            if (dc.driver.PageSource.Contains("name=\"maintenance_cost\""))
            {
                월관리비 = dc.driver.FindElementByName("maintenance_cost").GetAttribute("value");
            }

            if (dc.driver.PageSource.Contains("class=\"maintenance-costs-display"))
            {
                var rootOption = dc.driver.FindElementByClassName("maintenance-costs-display");
                var listOption = rootOption.FindElements(By.XPath("./span/i"));
                var listTarget = listOption.Where(x => x.GetAttribute("outerHTML").Contains(" on")).Select(y => y.Text.Trim());
                관리비포함내역 = string.Join(";", listTarget);
            }

        }
        public virtual void GetPropertyInfo()
        {
            if (dc.driver.PageSource.Contains("name=\"kind\""))
            {
                var elem = dc.driver.FindElementByName("kind");
                if (dc.IsContainAsOutHtml(elem, "지목"))
                    지목 = dc.GetSelectedValueFromDropdown(By.Name("kind"));
                else if (dc.IsContainAsOutHtml(elem, "건물종류"))
                    건물종류 = dc.GetSelectedValueFromDropdown(By.Name("kind"));
            }


            if (dc.driver.PageSource.Contains("name=\"area1\""))
            {
                var elem = dc.driver.FindElementByName("area1");
                if (dc.IsContainAsOutHtml(elem, "대지면적"))
                    대지면적 = elem.GetAttribute("value");
                else if (dc.IsContainAsOutHtml(elem, "공급면적"))
                    공급면적 = elem.GetAttribute("value");
                else if (dc.IsContainAsOutHtml(elem, "연면적"))
                    연면적 = elem.GetAttribute("value");
                else if (dc.IsContainAsOutHtml(elem, "계약면적"))
                    계약면적 = elem.GetAttribute("value");
            }

            if (dc.driver.PageSource.Contains("name=\"area2\""))
            {
                var elem = dc.driver.FindElementByName("area2");            
                전용면적 = elem.GetAttribute("value");

            }

            if (dc.driver.PageSource.Contains("name=\"keyword\""))
            {
                var elem = dc.driver.FindElementByName("keyword");
                건물명 = elem.GetAttribute("value");
            }

            if (dc.driver.PageSource.Contains("name=\"case_text1\""))
            {
                var elem = dc.driver.FindElementByName("case_text1");
                현업종 = elem.GetAttribute("value");
            }

            if (dc.driver.PageSource.Contains("name=\"case_item1\""))
            {
                용도 = dc.GetSelectedValueFromDropdown(By.Name("case_item1")).Trim();
            }

            if (dc.driver.PageSource.Contains("name=\"case_item3\""))
            {
                건축물용도 = dc.GetSelectedValueFromDropdown(By.Name("case_item3")).Trim();
            }

            if (dc.driver.PageSource.Contains("name=\"direction\""))
            {
                방향 = dc.GetSelectedValueFromDropdown(By.Name("direction")).Trim();
            }

            if (dc.driver.PageSource.Contains("name=\"rooms\""))
            {
                var elem = dc.driver.FindElementByName("rooms");
                방 = elem.GetAttribute("value");
            }

            if (dc.driver.PageSource.Contains("name=\"bathrooms\""))
            {
                var elem = dc.driver.FindElementByName("bathrooms");
                욕실 = elem.GetAttribute("value");
            }

            if (dc.driver.PageSource.Contains("name=\"households\""))
            {
                var elem = dc.driver.FindElementByName("households");
                총세대수 = elem.GetAttribute("value");
            }

            if (dc.driver.PageSource.Contains("name=\"using_date\""))
            {
                var elem = dc.driver.FindElementByName("using_date");
                건축물일자 = elem.GetAttribute("value");
            }

            if (dc.driver.PageSource.Contains("name=\"parking_volume\""))
            {
                var elem = dc.driver.FindElementByName("parking_volume");
                총주차대수 = elem.GetAttribute("value");
            }
        }
        public virtual void GetSpecialInfo()
        {
            간략소개 = dc.driver.FindElementByName("summary").GetAttribute("value");
            상세설명 = dc.driver.FindElementByName("feature").GetAttribute("value");
            메모 = dc.driver.FindElementByName("memo").GetAttribute("value");
        }

        public virtual void SetBasicInfo()
        {
            if (string.IsNullOrEmpty(매물종류) == false)
            {
                dc.SelectComboBox(dc.driver.FindElementByName("cateid"), 매물종류);
                Thread.Sleep(2000);
            }

            if (string.IsNullOrEmpty(거래유형) == false)
            {
                var listBtn = dc.driver.FindElementById("trade").FindElements(By.XPath("./span/i"));
                var btn = listBtn.FirstOrDefault(x=> dc.IsContainAsHtml(x , 거래유형));
                btn.Click();
                Thread.Sleep(1000);
            }

            InputAddressDetail();


            if (string.IsNullOrEmpty(동) == false)
            {
                dc.driver.FindElementByName("dong").SendKeys(동);
                Thread.Sleep(1000);
            }

            if (string.IsNullOrEmpty(호) == false)
            {
                dc.driver.FindElementByName("ho").SendKeys(호);
                Thread.Sleep(1000);
            }

            if (string.IsNullOrEmpty(지도표시) == false)
            {
                var listBtn = dc.driver.FindElementByClassName("map_state_option").FindElements(By.XPath("./i"));
                var btn = listBtn.FirstOrDefault(x => dc.IsContainAsHtml(x, 지도표시));
                btn.Click();
                Thread.Sleep(1000);
            }

            if (string.IsNullOrEmpty(해당층) == false)
            {
                dc.SetValue(By.Name("t_current_floor"), 해당층, true);
                Thread.Sleep(1000);
            }

            if (string.IsNullOrEmpty(총층) == false)
            {
                dc.SetValue(By.Name("t_total_floor") , 총층 , true);
                Thread.Sleep(1000);
            }

            if (string.IsNullOrEmpty(지상층) == false)
            {
                dc.driver.FindElementByName("total_floor").SendKeys(지상층);
                Thread.Sleep(1000);
            }

            if (string.IsNullOrEmpty(지하층) == false)
            {
                dc.driver.FindElementByName("current_floor").SendKeys(지하층);
                Thread.Sleep(1000);
            }

            if (string.IsNullOrEmpty(매매가) == false)
            {
                dc.driver.FindElementByName("sale_price").SendKeys(매매가);
                Thread.Sleep(1000);
            }

            if (string.IsNullOrEmpty(전세금) == false)
            {
                dc.driver.FindElementByName("deposit").SendKeys(전세금);
                Thread.Sleep(1000);
            }

            if (string.IsNullOrEmpty(보증금) == false)
            {
                dc.driver.FindElementByName("deposit").SendKeys(보증금);
                Thread.Sleep(1000);
            }

            if (string.IsNullOrEmpty(년세금) == false)
            {
                dc.driver.FindElementByName("monthly_rent").SendKeys(년세금);
                Thread.Sleep(1000);
            }

            if (string.IsNullOrEmpty(월세금) == false)
            {
                dc.driver.FindElementByName("monthly_rent").SendKeys(월세금);
                Thread.Sleep(1000);
            }

            if (string.IsNullOrEmpty(월관리비) == false)
            {
                dc.driver.FindElementByName("maintenance_cost").SendKeys(월관리비);
                Thread.Sleep(1000);

                if(string.IsNullOrEmpty(관리비포함내역) == false)
                {
                    var rootOption = dc.driver.FindElementByClassName("maintenance-costs-display");
                    var listOption = rootOption.FindElements(By.XPath("./span/i"));
                    foreach(var btn in listOption)
                    {
                        if (관리비포함내역.Contains(btn.Text.Trim()))
                        {
                            btn.Click();
                            Thread.Sleep(1000);
                        }
                    }
                }


                if (dc.driver.PageSource.Contains("부과 사유"))
                {
                    var rootFeeType = dc.driver.FindElementByCssSelector(".charge-fee-etc-type.cfbox.box_on");
                    var listFeeType = rootFeeType.FindElements(By.CssSelector(".radio.checking"));
                    listFeeType[0].Click();
                    Thread.Sleep(1000);

                    var rootChargeFee = dc.driver.FindElementByCssSelector(".cof-radio-group.sm.charge_fee");
                    var listCharge = rootChargeFee.FindElements(By.CssSelector(".radio.checking"));
                    listCharge[0].Click();
                    Thread.Sleep(1000);
                }             
            }
        }
        public virtual void SetPropertyInfo()
        {
            if (string.IsNullOrEmpty(지목) == false)
            {
                dc.SelectComboBox(dc.driver.FindElementByName("kind"), 지목);
                Thread.Sleep(2000);
            }

            if (string.IsNullOrEmpty(건물종류) == false)
            {
                dc.SelectComboBox(dc.driver.FindElementByName("kind"), 건물종류);
                Thread.Sleep(2000);
            }

            if (string.IsNullOrEmpty(대지면적) == false)
            {
                dc.driver.FindElementByName("area1").SendKeys(대지면적);
                Thread.Sleep(1000);
            }
            if (string.IsNullOrEmpty(공급면적) == false)
            {
                dc.SetValue(By.Name("area1"), 공급면적, true);
                Thread.Sleep(1000);
            }
            if (string.IsNullOrEmpty(연면적) == false)
            {
                dc.driver.FindElementByName("area1").SendKeys(연면적);
                Thread.Sleep(1000);
            }

            if (string.IsNullOrEmpty(계약면적) == false)
            {
                dc.SetValue(By.Name("area1"), 계약면적, true);
                Thread.Sleep(1000);
            }

            if (string.IsNullOrEmpty(전용면적) == false)
            {
                dc.SetValue(By.Name("area2"), 전용면적, true);
                Thread.Sleep(1000);
            }

            

            if (string.IsNullOrEmpty(건물명) == false)
            {
                dc.driver.FindElementByName("keyword").SendKeys(건물명);
                Thread.Sleep(1000);
            }

            if (string.IsNullOrEmpty(현업종) == false)
            {
                dc.driver.FindElementByName("case_text1").SendKeys(현업종);
                Thread.Sleep(1000);
            }

            if (string.IsNullOrEmpty(용도) == false)
            {
                dc.SelectComboBox(dc.driver.FindElementByName("case_item1"), 용도);
                Thread.Sleep(2000);
            }

            if (string.IsNullOrEmpty(건축물용도) == false)
            {
                dc.SelectComboBox(dc.driver.FindElementByName("case_item3"), 건축물용도);
                Thread.Sleep(2000);
            }

            if (string.IsNullOrEmpty(방향) == false)
            {
                dc.SelectComboBox(dc.driver.FindElementByName("direction"), 방향);
                Thread.Sleep(2000);
            }

            if (dc.driver.PageSource.Contains("입주가능일"))
            {
                dc.driver.FindElementByClassName("live_tab").FindElement(By.XPath("./li[1]")).Click();
                Thread.Sleep(1000);
            }

            if (string.IsNullOrEmpty(방) == false)
            {
                dc.SetValue(By.Name("rooms"), 방, true);
                Thread.Sleep(1000);
            }
            if (string.IsNullOrEmpty(욕실) == false)
            {
                dc.SetValue(By.Name("bathrooms"), 욕실, true);
                Thread.Sleep(1000);
            }

            if (string.IsNullOrEmpty(총세대수) == false)
            {
                dc.SetValue(By.Name("households"), 총세대수, true);
                Thread.Sleep(1000);
            }

            if (string.IsNullOrEmpty(건축물일자) == false)
            {
                dc.driver.FindElementByName("using_date").SendKeys(건축물일자);
                Thread.Sleep(1000);
            }

            if (string.IsNullOrEmpty(총주차대수) == false)
            {
                dc.SetValue(By.Name("parking_volume"), 총주차대수, true);
                Thread.Sleep(1000);
            }

        }
        public virtual void SetSpecialInfo()
        {
            if (string.IsNullOrEmpty(간략소개) == false)
            {
                dc.driver.FindElementByName("summary").SendKeys(간략소개);
                Thread.Sleep(1000);
            }
            
            if (string.IsNullOrEmpty(상세설명) == false)
            {
                dc.driver.FindElementByName("feature").SendKeys(상세설명);
                Thread.Sleep(1000);
            }
           
            if (string.IsNullOrEmpty(메모) == false)
            {
                dc.driver.FindElementByName("memo").SendKeys(메모);
                Thread.Sleep(1000);
            }  
            
            
        }

        public virtual void Process(PropertyInfo itemInfo_)
        {
            var title = $"{itemInfo_.번호}_{itemInfo_.제목}";

            var modify = string.Format(itemModifyUrl, itemInfo_.번호);
            dc.driver.Navigate().GoToUrl(modify);
            Thread.Sleep(1000);

            GetBasicInfo();
            GetPropertyInfo();
            GetSpecialInfo();

            string url = string.Format(itemModifyUrl, itemInfo_.번호);
            dc.driver.Navigate().GoToUrl(url);
            Thread.Sleep(1000);
            SaveData(title);

            //delete item
            Global.uiController.PrintLog($"{title} 아이템 제거");
            string urlDel = string.Format(itemUrl, itemInfo_.번호);
            dc.driver.Navigate().GoToUrl(urlDel);
            Thread.Sleep(1000);
            ClickDel();

            //add new item
            Global.uiController.PrintLog($"{title} 아이템 재등록");
            while (true)
            {
                dc.driver.Navigate().GoToUrl(regUrl);
                Thread.Sleep(1000);
#if !DEBUG
            try
#endif
                {
                    SetBasicInfo();
                    SetPropertyInfo();
                    SetSpecialInfo();
                    this.SetPicture($"DataFolder\\{title}\\image");

                    if (this.ClickRegConfirm())
                        break;
                }
#if !DEBUG
            catch
            {
                Global.uiController.PrintLog($"{title} 등록 오류. 재 등록 시도");
            }
#endif
            }
        }

        protected virtual void GetAddress()
        {
            매물소재지 = dc.driver.FindElementByName("addr_full").GetAttribute("value");

            if (dc.driver.PageSource.Contains("name=\"apt_size_idx\""))
            {
                면적 = dc.GetSelectedValueFromDropdown(By.Name("apt_size_idx"));
            }
        }

        protected void InputAddressChoiceDetail()
        {
            var listInfo = 매물소재지.Split(' ');

            dc.SelectComboBox(By.Name("select_area1"), listInfo[0]);
            dc.SelectComboBox(By.Name("select_area2"), listInfo[1]);
            dc.SelectComboBox(By.Name("select_area3"), listInfo[2]);

            dc.driver.FindElementByClassName("apt-noreg-btn-dis").Click();
            Thread.Sleep(1000);

            var rootEdit = dc.driver.FindElementByClassName("anlistbox");
            var jibun = rootEdit.FindElement(By.Name("apt_jibun"));
            jibun.SendKeys(listInfo[3]);
            Thread.Sleep(1000);
            var name = rootEdit.FindElement(By.Name("apt_name"));
            name.SendKeys(listInfo[4]);
            Thread.Sleep(1000);

            dc.driver.FindElementByClassName("complete-btn").Click();
            Thread.Sleep(1000);
        }

        protected virtual void InputAddressDetail()
        {
            dc.driver.FindElementById("jusoSearchButton").Click();
            Thread.Sleep(1000);
            dc.SetValue(By.Name("apt_search") , 매물소재지);

            dc.driver.FindElementByClassName("apt-search-btn").Click();
            Thread.Sleep(1000);
            var rootList = dc.driver.FindElementById("jpContainer_jusoSearchButtonapt-search-lay");
            var listOption = rootList.FindElements(By.XPath("./li"));

            var outerHTML = listOption[0].GetAttribute("outerHTML");
            if (outerHTML.Contains("단지를 찾을수 없어요"))
            {
                dc.driver.FindElementByXPath("//*[@id='jpContainer_jusoSearchButton']/ul/li[2]").Click();
                Thread.Sleep(1000);

                InputAddressChoiceDetail();
            }
            else
            {
                listOption[0].Click();
                Thread.Sleep(1000);
            }

            if (string.IsNullOrEmpty(면적) == false 
                && dc.driver.PageSource.Contains("name=\"apt_size_idx\""))
            {
                var elem = dc.driver.FindElementByName("apt_size_idx");
                if (elem.Displayed)
                    dc.SelectComboBoxByText(By.Name("apt_size_idx"), 면적);
            }
        }

        void SaveData(string path_)
        {
            var fullPath = AppDomain.CurrentDomain.BaseDirectory + "DataFolder\\" + path_;
            if (Directory.Exists(fullPath))
                Directory.Delete(fullPath, true);

            Directory.CreateDirectory(fullPath);

            //no have image 
            if (dc.driver.PageSource.Contains("cof-preview") == false)
                return;

            var rootImg = dc.driver.FindElementByClassName("cof-preview");
            var listImg = rootImg.FindElements(By.XPath("./li"));
            int i = 1;

            foreach (var item in listImg)
            {
                if (dc.IsContainAsHtml(item, "file.kcrwork.com") == false)
                    continue;

                var urlImg = item.FindElement(By.XPath("./img")).GetAttribute("data-original");
                urlImg = urlImg.Split('?')[0];

                if (Directory.Exists($"{fullPath}\\image") == false)
                    Directory.CreateDirectory($"{fullPath}\\image");

                System.Drawing.Bitmap b;
                Uri uri = new Uri(urlImg);
                WebRequest webRequest = (HttpWebRequest)WebRequest.Create(uri);
                using (HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse())
                {
                    using (Stream stream = webResponse.GetResponseStream())
                    {
                        System.Drawing.Image img = System.Drawing.Image.FromStream(stream);
                        img.Save($"{fullPath}\\image\\{i}.png");
                        img.Dispose();
                    }
                }
                ++i;
            }
        }
        void ClickDel()
        {

#if !DEBUG
            dc.driver.FindElementByXPath("//*[@id='contents']/div[2]/div/span[2]/a[4]").Click();
            Thread.Sleep(2000);
            dc.driver.FindElementById("fallr-buttons").FindElement(By.XPath("./a[2]")).Click();
            Thread.Sleep(1000);
            Thread.Sleep(2000);
            string msg = dc.IsAlertPresent();
            if (string.IsNullOrEmpty(msg) == false)
                Global.uiController.PrintLog(msg);
            Thread.Sleep(2000);
#endif
        }

        void SetPicture(string path_)
        {
            var fullPath = AppDomain.CurrentDomain.BaseDirectory + path_;
            if (Directory.Exists(fullPath) == false)
                return;
            var files = Directory.GetFiles(fullPath);
            foreach (var item in files)
            {
                //*[@id="file_image"]
                var btnImage = dc.driver.FindElementById("file_image");
                btnImage.SendKeys(item);
                Thread.Sleep(2000);
            }
        }
        bool ClickRegConfirm()
        {
            var rootForms = dc.driver.FindElementById("post-form").FindElements(By.ClassName("submit_area"));
            IWebElement rootBtn = null;
            foreach (var item in rootForms)
            {
                if (dc.IsContainAsHtml(item, "등록하기"))
                {
                    rootBtn = item;
                    break;
                }
            }
            rootBtn.FindElement(By.XPath("./input[2]")).Click();
            Thread.Sleep(3000);

            if (dc.driver.PageSource.Contains("매물등록이 완료 되었습니다."))
            {
                Global.uiController.PrintLog("매물등록 성공");
                return true;
            }
            else
            {
                Global.uiController.PrintLog("매물등록 실패...등록 정보를 확인해 주세요");
                return false;
            }
        }
    }
}
