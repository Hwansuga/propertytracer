﻿using OpenQA.Selenium;
using PropertyTracer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


class 상가건물 : PropertyBase
{
    protected override void InputAddressDetail()
    {
        dc.driver.FindElementById("jusoSearchButton").Click();
        Thread.Sleep(1000);
        dc.SetValue(By.Name("s_key"), 매물소재지);

        dc.driver.FindElementByClassName("search-addr-btn").Click();
        Thread.Sleep(1000);
        var rootList = dc.driver.FindElementById("jpContainer_jusoSearchButton_addrRows");
        var listOption = rootList.FindElements(By.XPath("./tr"));
        listOption[0].Click();
        Thread.Sleep(1000);

        var listJBOption = dc.driver.FindElementsByName("jibun_show_yn");
        listJBOption[1].Click();
        Thread.Sleep(1000);
    }
}

