﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PropertyTracer.Model
{
    class JejuallBase
    {
        public DriverController dc = null;

        public string 매물종류 { get; set; }
        public string 거래유형 { get; set; }
        public string 주소지1 { get; set; }
        public string 주소지2 { get; set; }
        public string 아파트 { get; set; }
        public string 주소지3 { get; set; }
        public string 매물명 { get; set; }
        public string 건물종류 { get; set; }
        public string 건축물용도 { get; set; }

        public string 공급면적 { get; set; }
        public string 전용면적 { get; set; }
        public string 대지면적 { get; set; }
        public string 연면적 { get; set; }

        public string 매매가 { get; set; }
        public string 전세가 { get; set; }
        public string 보증금 { get; set; }
        public string 년세 { get; set; }
        public bool 월세체크 { get; set; }
        public string 월세 { get; set; }
        public string 월관리비 { get; set; }
        public string 방향 { get; set; }
        public string 방수욕실수 { get; set; }
        public string 입주가능일 { get; set; }
        public string 건축물일자 { get; set; }

        public string 해당동 { get; set; }
        public string 해당층총층수 { get; set; }
        public string 총세대수 { get; set; }
        public string 총주차대수 { get; set; }
        public string 총주차대수2 { get; set; }
        public string 매물상세설명 { get; set; }

        public string 상호명 { get; set; }
        public string 담당자 { get; set; }
        public string 연락처1 { get; set; }
        public string 연락처2 { get; set; }
        public string 관리자메모 { get; set; }

        public Action actionDel = null;
        void GetBasicInfo()
        {
            //dc.RemoveDisplayNoneElem();

            매물종류 = GetSelectedValueFromDropdown(By.Name("cate_code")).Trim();
            거래유형 = GetSelectRadioGroup(By.Name("typechoice"));
            주소지1 = GetSelectedValueFromDropdown(By.Name("sido"));
            주소지2 = GetSelectedValueFromDropdown(By.Name("dong"));
            if (dc.driver.PageSource.Contains("name=\"apt\""))
                아파트 = dc.GetSelectedValueFromDropdown(By.Name("apt"));
            주소지3 = dc.driver.FindElementByName("roadFullAddr").GetAttribute("value");
            매물명 = dc.driver.FindElementByName("subject").GetAttribute("value");
            if (dc.driver.PageSource.Contains($"name=\"buil_type\""))
                건물종류 = GetSelectedValueFromDropdown(By.Name("buil_type")).Trim();
            if (dc.driver.PageSource.Contains($"name=\"buil_use\""))
                건축물용도 = GetSelectedValueFromDropdown(By.Name("buil_use")).Trim();


            공급면적 = dc.GetValueByName("sup_area_m");

            전용면적 = dc.GetValueByName("dedi_area_m");

            대지면적 = dc.GetValueByName("land_area_m");
            연면적 = dc.GetValueByName("exc_area_m");

            매매가 = dc.GetValueByName("bns_sale_price");

            전세가 = dc.GetValueByName("ct_deposit_money");

            보증금 = dc.GetValueByName("rt_down_payment");
            년세 = dc.GetValueByName("rt_year_pay");

            if (dc.driver.PageSource.Contains("name=\"month_pay\""))
                월세체크 = dc.GetCheckBox(By.Name("month_pay"));
            월세 = dc.GetValueByName("rt_month_pay");


            월관리비 = dc.GetValueByName("ct_month_expenses");

            방향 = dc.GetValueByName("diection");
            방수욕실수 = dc.GetValueByName("room_num");
            입주가능일 = dc.GetValueByName("movin_date");
            건축물일자 = dc.GetValueByName("completion");

            해당동 = dc.GetValueByName("house_num");
            해당층총층수 = dc.GetValueByName("floor");
            총세대수 = dc.GetValueByName("households_num");
            총주차대수 = dc.GetValueByName("parking");
            총주차대수2 = dc.GetValueByName("parking_num2");

            상호명 = dc.driver.FindElementByName("company_name").GetAttribute("value");
            담당자 = dc.driver.FindElementByName("company_person").GetAttribute("value");
            연락처1 = dc.driver.FindElementByName("company_tel").GetAttribute("value");
            연락처2 = dc.driver.FindElementByName("company_mobile").GetAttribute("value");

            관리자메모 = dc.GetValueByName("admin_info_content");

            매물상세설명 = dc.driver.FindElementByName("info_content").GetAttribute("value");
        }

        void SetBasicInfo()
        {
            SelectRadioGroup(By.Name("cate_code"), 매물종류);
            SelectRadioGroup(By.Name("typechoice"), 거래유형);

            dc.driver.FindElementByName("typedong").Click();
            Thread.Sleep(2000);

            dc.SelectComboBoxByValue(By.Name("sido") , 주소지1);
            dc.SetValue(By.Name("dong") , 주소지2);
            if (string.IsNullOrEmpty(아파트) == false)
                dc.SelectComboBox(By.Name("apt"), 아파트);

            dc.SetValue(By.Name("roadFullAddr"), 주소지3 , true);
            dc.driver.FindElementByName("map_yn").Click();
            Thread.Sleep(1000);

            dc.SetValue(By.Name("subject"), 매물명 , true);
            dc.SelectComboBoxByValue(By.Name("buil_type"), 건물종류);
            dc.SelectComboBoxByValue(By.Name("buil_use"), 건축물용도);
            dc.SetValue(By.Name("sup_area_m"), 공급면적 , true);
            dc.SetValue(By.Name("dedi_area_m"), 전용면적 , true);

            dc.SetValue(By.Name("land_area_m"), 대지면적, true);
            dc.SetValue(By.Name("exc_area_m"), 연면적, true);

            if (string.IsNullOrEmpty(매매가) == false)
                dc.SetValue(By.Name("bns_sale_price"), 매매가);
            if (string.IsNullOrEmpty(전세가) == false)
                dc.SetValue(By.Name("ct_deposit_money"), 전세가);
            if (string.IsNullOrEmpty(보증금) == false)
                dc.SetValue(By.Name("rt_down_payment"), 보증금);
            if (string.IsNullOrEmpty(년세) == false)
                dc.SetValue(By.Name("rt_year_pay"), 년세);

            dc.SetCheckBox(By.Name("month_pay"), 월세체크);
            dc.SetValue(By.Name("rt_month_pay"), 월세);


            dc.SetValue(By.Name("ct_month_expenses"), 월관리비);

            dc.SetValue(By.Name("diection"), 방향);
            dc.SetValue(By.Name("room_num"), 방수욕실수);
            dc.SetValue(By.Name("movin_date"), 입주가능일);
            dc.SetValue(By.Name("completion"), 건축물일자);

            dc.SetValue(By.Name("house_num"), 해당동);
            dc.SetValue(By.Name("floor"), 해당층총층수);
            dc.SetValue(By.Name("households_num"), 총세대수 , true);
            dc.SetValue(By.Name("parking"), 총주차대수 , true);
            dc.SetValue(By.Name("parking_num2"), 총주차대수2 , true);

            /*
            dc.SetValue(By.Name("company_name"), 상호명);
            dc.SetValue(By.Name("company_person"), 담당자);
            dc.SetValue(By.Name("company_tel"), 연락처1);
            dc.SetValue(By.Name("company_mobile"), 연락처2);
            */

            dc.SetValue(By.Name("admin_info_content"), 관리자메모, true);
        }

        public void GetProcess(string _title)
        {
            Global.uiController.PrintLog($"{_title} 아이템 정보 수집");
            GetBasicInfo();

            SaveImg(_title);
        }

        public void SetProcess(string _title)
        {
            Global.uiController.PrintLog($"{_title} 아이템 재등록");
            while (true)
            {
                dc.driver.Navigate().GoToUrl("https://www.jejuall.com/CMyProperty/write");
                Thread.Sleep(1000);

                while (dc.driver.PageSource.Contains("captcha-table"))
                {
                    Global.uiController.PrintLog("capcha 감지");

                    Thread.Sleep(1000 * 50 * 30);

                    dc.driver.Navigate().Refresh();
                    var msg = dc.IsAlertPresent();
                    Global.uiController.PrintLog("msg");
                }

#if !DEBUG
                try
#endif
                {
                    SetBasicInfo();
                    SetImg($@"jejuall\{_title}");

                    //*[@id="saveForm"]/table/tbody/tr[24]/td[2]/iframe
                    dc.driver.SwitchTo().Frame(dc.driver.FindElementByCssSelector("iframe[frameborder='0']"));
                    dc.driver.SwitchTo().Frame(dc.driver.FindElementById("se2_iframe"));
                    var script = $@"document.getElementsByTagName('body')[0].innerHTML = `{매물상세설명}`;";
                    dc.driver.ExecuteScript(script);
                    dc.driver.SwitchTo().DefaultContent();

                    if (this.ClickRegConfirm())
                        break;
                }
#if !DEBUG
                catch
                {
                    Global.uiController.PrintLog($"{_title} 등록 오류. 재 등록 시도");
                }
#endif
            }
        }
        private void SaveImg(string title)
        {
            var fullPath = $@"{AppDomain.CurrentDomain.BaseDirectory}jejuall\{title}\";
            FileInfo fileInfo = new FileInfo(fullPath);
            if (fileInfo.Directory.Exists)
                fileInfo.Directory.Delete(true);

            fileInfo.Directory.Create();
            var wc = new WebClient();
            var listImg = dc.driver.FindElementById("sortable").FindElements(By.XPath("./li"));
            int i = 0; 
            foreach(var item in listImg)
            {
                var url = item.FindElement(By.XPath("./img")).GetAttribute("src");
                wc.DownloadFile(url, $@"{fullPath}\{i}.jpg");
                i++;
            }


            //https://www.jejuall.com/uploadFile_Thumb/sunsun3832/240722185318p695_n9y_thumb.jpg
            //https://www.jejuall.com/uploadFile_Thumb/sunsun3832/240722185318p695_n9y_thumb.jpg
        }

        void SetImg(string path)
        {
            var fullPath = $@"{AppDomain.CurrentDomain.BaseDirectory}{path}";
            var files = Directory.GetFiles(fullPath);
            foreach (var item in files)
            {
                //*[@id="file_image"]
                //*[@id="uploadifive-car_img"]/input[2]
                var uploadRoot = dc.driver.FindElementById("uploadifive-car_img");
                var btnImage = uploadRoot.FindElement(By.CssSelector("input[type='file'][multiple='multiple']:not([data-gtm-form-interact-field-id]"));
                btnImage.SendKeys(item);
                Thread.Sleep(2000);
            }
        }

        string GetSelectedValueFromDropdown(By _by)
        {
            var select = new SelectElement(dc.driver.FindElement(_by));
            var option = select.SelectedOption;
            return option.GetAttribute("value");
        }

        string GetSelectRadioGroup(By _by)
        {
            var listOption = dc.driver.FindElements(_by);
            var target = listOption.FirstOrDefault(x => dc.IsContainAsOutHtml(x,"checked"));
            return target.GetAttribute("value");
        }

        void SelectRadioGroup(By _by, string _item)
        {
            var listOption = dc.driver.FindElements(_by);
            var target = listOption.FirstOrDefault(x => x.GetAttribute("value").Contains(_item));
            target.Click();
            Thread.Sleep(2000);
        }

        bool ClickRegConfirm()
        {
            // /html/body/div[1]/section/div[1]/div[2]/div[2]/div/div
            // /html/body/div[1]/section/div[1]/div[2]/div[2]/div/div/div[1]/a[2]
            var rootBtn = dc.driver.FindElementByClassName("btnbox");
            var btnReg = rootBtn.FindElement(By.XPath("./div[1]/a[2]"));
            btnReg.Click();
            Thread.Sleep(5000);

            if (dc.driver.PageSource.Contains("<h1 class=\"commtitle\">매물관리</h1>"))
            {
                Global.uiController.PrintLog("매물등록 성공");
                return true;
            }
            else
            {
                Global.uiController.PrintLog("매물등록 실패...등록 정보를 확인해 주세요");
                return false;
            }
        }
    }
}
