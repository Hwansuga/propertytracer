﻿using OpenQA.Selenium;
using PropertyTracer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


class 토지_임야 : PropertyBase
{
    public string 지번표시 { get; set; }

    protected override void GetAddress()
    {
        base.GetAddress();
        //var listJBOption = dc.driver.FindElementsByName("jibun_show_yn");
        //foreach(var item in listJBOption)
        //{

        //}
    }
    protected override void InputAddressDetail()
    {
        dc.driver.FindElementById("jusoSearchButton").Click();
        Thread.Sleep(1000);

        dc.driver.FindElementByXPath("//*[@id='jpContainer_jusoSearchButton']/ul/li[2]").Click();
        Thread.Sleep(1000);

        var listAddr = 매물소재지.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries);
        dc.SelectComboBoxByText(By.Name("select_area1"), listAddr[0]);
        dc.SelectComboBoxByText(By.Name("select_area2"), listAddr[1]);
        dc.SelectComboBoxByText(By.Name("select_area3"), listAddr[2]);
        if (listAddr.Length >= 5)
        {
            dc.SelectComboBoxByText(By.Name("li_select"), listAddr[3]);
            dc.SetValue(By.Name("jibun"), listAddr[4]);
        }
        else
        {
            dc.SetValue(By.Name("jibun"), listAddr[3]);
        }

        
        dc.driver.FindElementByClassName("select-addr-btn").Click();
        Thread.Sleep(1000);

        var rootList = dc.driver.FindElementByClassName("juso_list");
        var listOption = rootList.FindElements(By.ClassName("choose"));
        listOption[0].Click();
        Thread.Sleep(1000);

        var listJBOption = dc.driver.FindElementsByName("jibun_show_yn");
        listJBOption[1].Click();
        Thread.Sleep(1000);

        /*
        if (string.IsNullOrEmpty(지번표시) == false)
        {
            var listJBOption = dc.driver.FindElementsByName("jibun_show_yn");
            foreach (var item in listJBOption)
            {
            }
        }
        */
    }
}

